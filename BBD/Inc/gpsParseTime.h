#ifndef gpsParseTime_H_INCLUDED
#define gpsParseTime_H_INCLUDED
#include <stdint.h>
//define nmea types
#define GGA "$GPGGA"
typedef struct nmeaGPS{
	uint8_t time[5];
	uint8_t latitude[8];
	uint8_t longtitude[10];
	uint8_t fix;
} GPS;
uint8_t* getTime(void);
uint8_t* getLatitude(void);
uint8_t getFix(void);
void gpsParseTime(uint8_t* nmea,uint32_t size);
#endif
