#ifndef gpsParseLatitude_H_INCLUDED
#define gpsParseLatitude_H_INCLUDED
#include <stdint.h>
//define nmea types
#define GGA "$GPGGA"
uint8_t* getLat(void);
void gpsParseLat(uint8_t* nmea);
#endif
