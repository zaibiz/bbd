#ifndef gpsParseLong_H_INCLUDED
#define gpsParseLong_H_INCLUDED
#include <stdint.h>
//define nmea types
#define GGA "$GPGGA"
uint8_t* getLongt(void);
void gpsParseLong(uint8_t* nmea);
#endif
