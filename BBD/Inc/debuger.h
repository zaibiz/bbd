#ifndef debuger_H_INCLUDED
#define debuger_H_INCLUDED
#include <stdint.h>
#include "stm32f1xx_hal.h"

void writeln(uint8_t* string);
void writelnc(char* charstring);
int strlength(char s[]);
void MX_USART3_UART_Init(void);

#endif
