#include "gpsParseLatitude.h"
#include <string.h>
#include "debuger.h"

//variables
int gpsMode2=0;
uint8_t latitude[7];

//GPS parsing function
void gpsParseLat(uint8_t* nmea){
	if(!memcmp(nmea,GGA,6)) 
	{
			*latitude=NULL;
		
		//writeln(nmea);
		int j=0;
		int i=7;
		for(i=7;i<100;i++)
		{
			switch (nmea[i])
			{
				case ',' :j=0;
									gpsMode2++;
									break;
				case '*' : gpsMode2=99;
									 break;
				default : if(gpsMode2==1)
									{
										latitude[j]=nmea[i];
										j++;
									}
									break;
			}
			if (gpsMode2>1) break;
		}
		
	}
	gpsMode2=0;
	writeln(latitude);
}

uint8_t* getLat(){
	return latitude;
}
