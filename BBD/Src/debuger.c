#include "debuger.h"
int strlength(char s[]);
UART_HandleTypeDef huart3;

void MX_USART3_UART_Init(void){

  huart3.Instance = USART3;
  huart3.Init.BaudRate = 115200;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  HAL_UART_Init(&huart3);

}
void writeln(uint8_t* string){
	if(string[0]==0x0D) return;
	char* debugLine = (char*)string;
	writelnc(debugLine);
	writelnc("\r");
}
void writelnc(char charstring[]){
	int n=strlength(charstring);
	uint8_t* string = (uint8_t*)charstring;
	HAL_UART_Transmit_DMA(&huart3,string,n);
	__HAL_UART_FLUSH_DRREGISTER(&huart3);
}

int strlength(char s[]) {
   int c = 0;
 
   while (s[c] != '\0')
      c++;
 
   return c;
}
