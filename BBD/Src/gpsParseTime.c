#include "gpsParseTime.h"
#include <string.h>
#include "debuger.h"
typedef int bool;
enum { false, true };
#define gps_time 6
#define gps_latitude 8
#define gps_longtitude 9
#define gps_fix 1
//variables
int gpsMode1=0;
GPS gps;
//GPS parsing function
void gpsParseTime(uint8_t* nmea,uint32_t size){
	uint8_t comma=',';
	char *addr;
	char *addr1;
	if(!memcmp(nmea,GGA,6)) 
	{
		writeln(nmea);
		addr=memchr(nmea,comma,size);
		addr++;
		addr1=memchr(addr,comma,size);
		addr1++;
		memcpy(gps.time,addr,gps_time);
		memcpy(&gps.latitude[1],addr1,gps_latitude);
		return;
	}
}

uint8_t* getTime(){
	return gps.time;
}
uint8_t* getLatitude(){
	return gps.latitude;
}
uint8_t getFix(void){
	return gps.fix;
}
